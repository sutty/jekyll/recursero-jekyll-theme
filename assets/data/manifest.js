---
# Con esto le indicamos a Jekyll que el archivo es JS pero antes lo
# vamos a pasar por Liquid
#
# Genera un Array de URLs con su hash, de forma que podamos pasarle la
# lista al ServiceWorker
#
# Los guiones son para poder escribir con indentación pero eliminar todo
# el espacio vacío al generar el archivo.
---
const MANIFEST = [
  {%- for files in site.manifest -%}
    {%- for file in site[files] -%}
      {%- if file.url contains '.mp4' -%}{%- continue -%}{%- endif -%}
      {%- unless file.url -%}
        {%- assign path = file.path | remove_first: './' | remove_first: '/' -%}

        {%- comment -%}
          ForkAwesome tiene la versión en la URL y si no la agregamos no
          queda en la pre caché.
        {%- endcomment -%}
        {%- if path contains 'forkawesome-webfont' -%}
          {%- assign path = path | append: '?v=1.1.7' -%}
        {%- endif -%}
      {%- endunless -%}
      { url: "{{ file.url | default: path }}", revision: {% if file.hash %}"{{ file.hash }}"{% else %}null{% endif %} },
    {%- endfor -%}
  {%- endfor -%}
]
