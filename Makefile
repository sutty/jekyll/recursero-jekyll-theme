SHELL := bash
.DEFAULT_GOAL := help

site ?= $(patsubst %-jekyll-theme,%,$(notdir $(PWD)))
domain ?= $(site).sutty.local
env ?= development
hainish ?= ../haini.sh/haini.sh
hain ?= ../hain

JEKYLL_ENV := $(env)

# Copiar el archivo de configuración y avisar cuando hay que
# actualizarlo.
.env: .env.example
	@test -f $@ || cp -v $< $@
	@test -f $@ && echo "Revisa $@ para actualizarlo con respecto a $<"
	@test -f $@ && diff -auN --color $@ $<

# Incluir la configuracion
include .env

export

help: always ## Ayuda
	@grep "^[^\t]\+:.*##" Makefile | sed -re "s/(.*):.*##(.*)/\1;\2/" | column -s ";" -t | sed -re "s/^([^ ]+) /\x1B[38;5;197m\1\x1B[0m/"

all: install fa serve build ## Todas las tareas necesarias para desarrollar

install: node_modules Gemfile.lock ## Instalar las dependencias

build: ## Compilar el sitio (env=production)
	make bundle args="exec jekyll build --profile --trace"
	@echo -e "\a"

sassdoc: _sassdoc/index.html ## Genera la documentación de SCSS

hain: ## Correr comandos con hain (args="comandos")
	$(hainish) '$(args)'

bundle: ## Correr comandos con bundler (args="install")
	$(MAKE) hain args="bundle $(args)"

yarn: ## Correr comandos con yarn (args="add paquete")
	$(MAKE) hain args="yarn $(args)"

npx: ## Correr comandos con npx (args="run")
	$(MAKE) hain args="npx $(args)"

npm: ## Correr comandos con npm (args="install -g paquete")
	$(MAKE) hain args="npm $(args)"

serve: /etc/hosts $(hain)/run/nginx/nginx.pid ## Servidor de desarrollo
	@echo "Iniciado servidor web en https://$(domain):4000/"

webpack: assets/js/pack.js ## Compilar JS
	@echo -e "\a"

webpack-dev-server: $(hain)/tmp/webpack.pid ## Servidor de prueba de Webpack

fa: assets/fonts/forkawesome-webfont.woff2 ## Fork Awesome
	@echo -e "\a"

fa-%: always ## Agregar un ícono de Fork Awesome (reemplazar % por el nombre)
	@echo Agregando $* a la lista de íconos
	@grep -q "$*" fa.txt || grep '^$$fa-var-$*: ' node_modules/fork-awesome/scss/_variables.scss | sed -re "s/^.*: \"\\\(.*)\";/\1 $*/" >> fa.txt

gfont: ## Descarga tipografías de Google Fonts (css="url del css")
	@test -n "$(css)"
	wget "$(css)" -nv -kO /tmp/gfont
	cd assets/fonts/ && grep -Eo "https://[^\"')]+" /tmp/gfont | sort -u | wget -i - -x -nH -nv --cut-dir 1 -N
	grep -q "$(css)" _sass/fonts.scss || sed -r -e "1i // $(css)" -e "s,https://fonts\.gstatic\.com/s/,../fonts/," -e "s/\.[to]tf/-subset.woff2/g" -e "s/(true|open)type/woff2/g" /tmp/gfont >> _sass/fonts.scss
	find assets/fonts/ -name "*.?tf" | sed -re "s/\.[ot]tf/-subset.woff2/" | xargs make

push: ## Publica los cambios locales
	sudo chgrp -R 82 _site
	rsync -avi --delete-after _site/ root@athshe.sutty.nl:/srv/sutty/srv/http/data/_deploy/$(site).sutty.nl/

node_modules: package.json
	$(MAKE) yarn

Gemfile.lock: Gemfile
	$(MAKE) bundle args=install

# XXX: Cada vez que se reinicia el sistema, cambia la fecha de creación
# del pid 1 y lo usamos como medida para saber si hay que iniciar nginx.
# Si nginx se cae después de iniciarlo, no vamos a reiniciarlo así.
$(hain)/run/nginx/nginx.pid: /proc/1
	$(MAKE) hain args=nginx

$(hain)/tmp/webpack.pid:
	$(MAKE) hain args="./node_modules/.bin/webpack-dev-server --public $(domain):4000 --host 127.0.0.1 --port 65001 & echo $$! > /tmp/webpack.pid"

/etc/hosts: always
	@echo "Chequeando si es necesario agregar el dominio local $(domain)"
	@grep -q " $(domain)$$" $@ || echo -e "127.0.0.1 $(domain)\n::1 $(domain)" | sudo tee -a $@

js = $(wildcard _packs/*.js) $(wildcard _packs/*/*.js) $(wildcard *.js)
assets/js/pack.js: $(js)
	$(MAKE) hain args="./node_modules/.bin/webpack --config webpack.prod.js"

# Tomar los códigos de los íconos de este archivo y copiarlos a fa.txt
# node_modules/font-awesome/scss/_variables.scss
assets/fonts/forkawesome-webfont.woff2: fa.txt
	grep -v "^#" fa.txt | sed "s/^/U+/" | cut -d " " -f 1 | tr "\n" "," | xargs -rI {} make hain args="pyftsubset node_modules/fork-awesome/fonts/forkawesome-webfont.ttf --output-file=$@ --unicodes={} --layout-features='*' --flavor=woff2"

# Rangos Unicode para alfabeto latino.
latin_unicode := "U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD"

# Generar woff2 reducidas al alfabeto latino
%-subset.woff2: %.ttf
	make hain args="pyftsubset $< --output-file=$@ --unicodes=$(latin_unicode) --layout-features='*' --flavor=woff2"

# Generar woff2 reducidas al alfabeto latino
%-subset.woff2: %.otf
	make hain args="pyftsubset $< --output-file=$@ --unicodes=$(latin_unicode) --layout-features='*' --flavor=woff2"

sass_files := $(wildcard _sass/*.scss) assets/css/styles.scss .sassdocrc
_sassdoc/index.html: yarn $(sass_files)
	./node_modules/.bin/sassdoc _sass/ assets/css/ --dest ./_sassdoc

.PHONY: always
