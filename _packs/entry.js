import { Notifier } from '@airbrake/browser'

window.airbrake = new Notifier({
  projectId: window.env.AIRBRAKE_PROJECT_ID,
  projectKey: window.env.AIRBRAKE_PROJECT_KEY,
  host: 'https://panel.sutty.nl'
})

import 'core-js/stable'
import 'regenerator-runtime/runtime'

// Turbo acelera la navegación al no tener que recargar todo el JS y CSS
// de la página, con lo que se siente más rápida y "nativa".
//
// Cambiamos de turbolinks a turbo porque turbo soporta la función
// fetch(), que luego es interceptada por el SW para obtener las
// direcciones localmente.
import * as Turbo from "@hotwired/turbo"
Turbo.start()

// StimulusJS.org
import { Application } from 'stimulus'
import { definitionsFromContext } from "stimulus/webpack-helpers"

const application = Application.start()
const context = require.context("./controllers", true, /\.js$/)
application.load(definitionsFromContext(context))

try {
  window.axe = require('axe-core/axe')
} catch(e) {}

if (window.axe) window.axe.configure({ locale: require('axe-core/locales/es.json') })

document.addEventListener('turbo:load', event => {
  if (!window.axe) return

  window.axe.run().then(results => {
    results.violations.forEach(violation => {
      violation.nodes.forEach(node => {
        node.target.forEach(target => {
          document.querySelectorAll(target).forEach(element => {
            element.classList.add('inaccesible')
            element.ariaLabel = node.failureSummary
          })
        })
      })
    })
  })
})
