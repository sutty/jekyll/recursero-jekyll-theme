// TODO: Incorporar core-js?
import 'regenerator-runtime/runtime'

// Este archivo tiene el listado de direcciones y sus hashes para poder
// cachear y es generado por Jekyll
importScripts('assets/data/manifest.js')

// Por ahora obtenemos todas las URLs que no están repetidas
const urls = MANIFEST.map(x => x.url).filter((x, i, a) => a.indexOf(x) === i)
const urlNoCache = [ /\.mp4$/ ]

// XXX: No hacer nada durante la instalación
self.addEventListener('install', event => {})

// Cuando se activa el SW aunque no se haya actualizado, descargamos
// todas las páginas del manifest.  Así tomamos control del sitio más
// rápido y actualizamos las direcciones que aparezcan nuevas después de
// la instalación del SW.
self.addEventListener('activate', event => {
  event.waitUntil(
    caches.open('CACHE').then(cache => {
      urls.forEach(async url => {
        const matches = await cache.match(url)

        if (!matches) cache.put(url, await fetch(url))
      })
    })
  )
})

// Intentar primero obtener de la red.  Si Internet funciona,
// actualizamos la caché.  Si no, traemos lo que haya.
//
// @return [Response]
const fetchAndOrCache = async (request) => {
  let response

  try {
    response = await fetch(request)

    if (response.ok) {
      const cache = await caches.open('CACHE')

      cache.put(request, response.clone())
    }
  } catch {
    response = await caches.match(request)
  }

  // XXX: Si no hay red y no está cacheado, deberíamos responder con un
  // error temporal o una página de informativa?
  return response
}

// Interceptar la descarga de URLs.  Cuando Turbo intenta visitar una
// URL, el SW intercepta la Request.
self.addEventListener('fetch', event => {
  // No interceptar envío de formularios
  if (event.request.method !== 'GET') return

  // No cache estos archivos, usar la caché del servidor
  for (const noCache of urlNoCache) {
    if (event.request.url.match(noCache)) return
  }

  event.respondWith(fetchAndOrCache(event.request).then(r => r))
})
