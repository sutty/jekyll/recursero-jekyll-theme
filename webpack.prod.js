const webpack = require('webpack')
const merge = require('webpack-merge')
const common = require('./webpack.config')
const WebpackAssetsManifest = require('webpack-assets-manifest')

module.exports = merge.merge(common, {
  mode: 'production',
  output: {
    filename: '[name].[hash].js'
  },
  devtool: 'source-map',
  plugins: [
    new webpack.IgnorePlugin(/axe-core/),
    new WebpackAssetsManifest({
      output: '_data/assets.json'
    })
  ]
});
