# frozen_string_literal: true

Gem::Specification.new do |spec|
  # Este es el nombre de la gema
  spec.name          = 'recursero-jekyll-theme'
  # Versión inicial, usamos semver.org
  spec.version       = '0.3.0'
  # Una lista con todes les autores, esto se ve luego en el perfil de
  # rubygems.org
  spec.authors       = ['f']
  # Una lista de direcciones de correo, que coincide con la lista de
  # autores
  spec.email         = ['f@sutty.nl']

  # Descripción corta de la plantilla
  spec.summary       = 'A theme for resource toolkits'
  spec.description   = "Theme used in https://recursero.info/ inspired by CloudCannon's base-jekyll-template"
  # Dirección de la plantilla
  spec.homepage      = 'https://0xacab.org/sutty/jekyll/recursero-jekyll-theme'
  # Estamos usando la licencia MIT Antifacista, que no es reconocida por
  # rubygems
  spec.license       = 'Nonstandard'

  # La gemspec por defecto incluye todos los archivos que ponemos en
  # git, pero como no todos los que pongamos son necesarios para la
  # gema, hacemos nuestra propia lista de archivos.
  spec.files         = Dir['assets/**/*',
                           '_layouts/**/*',
                           '_includes/**/*',
                           '_sass/**/*',
                           '_data/**/*',
                           '_config.yml',
                           'LICENSE*',
                           'README*']

  # Archivos que son parte de la documentación
  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  # Opciones para el generador de documentación
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  # Otros metadatos de la gema
  spec.metadata = {
    'bug_tracker_uri'   => "#{spec.homepage}/issues",
    'homepage_uri'      => spec.homepage,
    'source_code_uri'   => spec.homepage,
    'changelog_uri'     => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  # Dependencias, esto se va a instalar al instalar la gema
  spec.add_runtime_dependency 'jekyll', '~> 4'
  spec.add_runtime_dependency 'jekyll-relative-urls', '~> 0'
  spec.add_runtime_dependency 'jekyll-seo-tag', '~> 2'
  spec.add_runtime_dependency 'jekyll-images', '~> 0.2'
  spec.add_runtime_dependency 'jekyll-include-cache', '~> 0'
  spec.add_runtime_dependency 'jekyll-data', '~> 1.1'
  spec.add_runtime_dependency 'sutty-archives', '~> 2.2'
  spec.add_runtime_dependency 'jekyll-lunr', '~> 0.1'
  spec.add_runtime_dependency 'jekyll-unique-urls', '~> 0.1'
  spec.add_runtime_dependency 'jekyll-locales', '~> 0.1'
  spec.add_runtime_dependency 'jekyll-linked-posts', '~> 0'
  spec.add_runtime_dependency 'jekyll-order', '~> 0'
  spec.add_runtime_dependency 'sutty-liquid', '~> 0'
  spec.add_runtime_dependency 'jekyll-commonmark', '~> 1.3'
  spec.add_runtime_dependency 'jekyll-dotenv', '>= 0.2'
  spec.add_runtime_dependency 'jekyll-feed', '~> 0.15'
  spec.add_runtime_dependency 'jekyll-ignore-layouts', '~> 0'

  # Dependencias de desarrollo
  spec.add_development_dependency 'bundler', '~> 2.1'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'pry', '~> 0'
end
